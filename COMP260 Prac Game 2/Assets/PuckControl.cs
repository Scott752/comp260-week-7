﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(AudioSource))]
public class PuckControl : MonoBehaviour {

	public AudioClip wallCollideClip;
	private AudioSource audio;
	public AudioClip paddleCollideClip;
	public LayerMask paddleLayer;

	public Transform startingPos;
	private Rigidbody rigidbody;

	void OnCollisionEnter(Collision collision) {
		Debug.Log("Collision Enter" + collision.gameObject.name);
		if (paddleLayer.Contains (collision.gameObject)) {
			
			audio.PlayOneShot (paddleCollideClip);
		} else {
			audio.PlayOneShot (wallCollideClip);
		}
	}


	void OnCollisionStay(Collision collision) {
		Debug.Log("Collision Stay" + collision.gameObject.name);
	}

	void OnCollisionExit(Collision collision) {
		Debug.Log("Collision Exit" + collision.gameObject.name);
	}






		void Start () {
			audio = GetComponent<AudioSource> ();
			rigidbody = GetComponent<Rigidbody>();
			ResetPosition();

		}

public void ResetPosition() {
	// teleport to the starting position
	rigidbody.MovePosition(startingPos.position);
}

	
	// Update is called once per frame
	void Update () {
	
	}
}
