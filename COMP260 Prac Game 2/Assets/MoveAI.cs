﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class MoveAI : MonoBehaviour {

	public Vector2 move, direction;
	public float maxSpeed = 5.0f;
	private Rigidbody rigidbody;
	public Rigidbody puck;
	public Transform ownGoal;
	//public Transform Puck; 
	public float speed = 3f; 

	void Start () {
		rigidbody = GetComponent<Rigidbody> ();
		//MoveToPuck ();
	}



	// Update is called once per frame
	void Update () {

	}


	void FixedUpdate(){
		//move back to the goal area

		if(puck.position.x > 0 && puck.position.y <= 2 && puck.position.y >= -2){
			//if puck enters trigger then move towrds puck
			Vector3 targ = puck.position;
			Vector3 dir = targ - rigidbody.position; 
			Vector3 vel = speed * dir.normalized;
			rigidbody.velocity = vel;
		}else{
			//move back towrds the goal
			Vector3 targ = ownGoal.position;
			Vector3 dir = targ - rigidbody.position;
			Vector3 vel = speed * dir.normalized;
			rigidbody.velocity = vel;

		}

		//transform.Translate (move);

	}
}


